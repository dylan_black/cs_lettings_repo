﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using ClearSkyLettings.Models;

namespace ClearSkyLettings.Data
{
    public class ClearSkyLettingsEnquiryContext : DbContext
    {
        public ClearSkyLettingsEnquiryContext (DbContextOptions<ClearSkyLettingsEnquiryContext> options)
            : base(options)
        {
        }

        public DbSet<ClearSkyLettings.Models.Enquiry> Enquiry { get; set; }
    }
}
