﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace ClearSkyLettings.Migrations.ClearSkyLettingsEnquiry
{
    public partial class EnquiryMigration07 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Enquiry_Property_PropertyID",
                table: "Enquiry");

            migrationBuilder.DropIndex(
                name: "IX_Enquiry_PropertyID",
                table: "Enquiry");

            migrationBuilder.DropColumn(
                name: "PropertyID",
                table: "Enquiry");

            migrationBuilder.AddColumn<int>(
                name: "PropertyObjectID",
                table: "Enquiry",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Enquiry_PropertyObjectID",
                table: "Enquiry",
                column: "PropertyObjectID");

            migrationBuilder.AddForeignKey(
                name: "FK_Enquiry_Property_PropertyObjectID",
                table: "Enquiry",
                column: "PropertyObjectID",
                principalTable: "Property",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Enquiry_Property_PropertyObjectID",
                table: "Enquiry");

            migrationBuilder.DropIndex(
                name: "IX_Enquiry_PropertyObjectID",
                table: "Enquiry");

            migrationBuilder.DropColumn(
                name: "PropertyObjectID",
                table: "Enquiry");

            migrationBuilder.AddColumn<int>(
                name: "PropertyID",
                table: "Enquiry",
                type: "int",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Enquiry_PropertyID",
                table: "Enquiry",
                column: "PropertyID");

            migrationBuilder.AddForeignKey(
                name: "FK_Enquiry_Property_PropertyID",
                table: "Enquiry",
                column: "PropertyID",
                principalTable: "Property",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
