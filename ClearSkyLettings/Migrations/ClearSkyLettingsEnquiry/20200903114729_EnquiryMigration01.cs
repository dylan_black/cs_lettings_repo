﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace ClearSkyLettings.Migrations.ClearSkyLettingsEnquiry
{
    public partial class EnquiryMigration01 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "propertyID",
                table: "Enquiry",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "Property",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Title = table.Column<string>(nullable: true),
                    ListingDate = table.Column<DateTime>(nullable: false),
                    AvailableDate = table.Column<DateTime>(nullable: false),
                    Type = table.Column<string>(nullable: true),
                    Beds = table.Column<int>(nullable: false),
                    Bathrooms = table.Column<int>(nullable: false),
                    Rent = table.Column<decimal>(type: "decimal(18,2)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Property", x => x.ID);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Enquiry_propertyID",
                table: "Enquiry",
                column: "propertyID");

            migrationBuilder.AddForeignKey(
                name: "FK_Enquiry_Property_propertyID",
                table: "Enquiry",
                column: "propertyID",
                principalTable: "Property",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Enquiry_Property_propertyID",
                table: "Enquiry");

            migrationBuilder.DropTable(
                name: "Property");

            migrationBuilder.DropIndex(
                name: "IX_Enquiry_propertyID",
                table: "Enquiry");

            migrationBuilder.DropColumn(
                name: "propertyID",
                table: "Enquiry");
        }
    }
}
