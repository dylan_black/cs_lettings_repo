﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace ClearSkyLettings.Migrations.ClearSkyLettingsEnquiry
{
    public partial class EnquiryMigration03 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Enquiry_Property_propertyID",
                table: "Enquiry");

            migrationBuilder.RenameColumn(
                name: "propertyID",
                table: "Enquiry",
                newName: "PropertyID");

            migrationBuilder.RenameIndex(
                name: "IX_Enquiry_propertyID",
                table: "Enquiry",
                newName: "IX_Enquiry_PropertyID");

            migrationBuilder.AddForeignKey(
                name: "FK_Enquiry_Property_PropertyID",
                table: "Enquiry",
                column: "PropertyID",
                principalTable: "Property",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Enquiry_Property_PropertyID",
                table: "Enquiry");

            migrationBuilder.RenameColumn(
                name: "PropertyID",
                table: "Enquiry",
                newName: "propertyID");

            migrationBuilder.RenameIndex(
                name: "IX_Enquiry_PropertyID",
                table: "Enquiry",
                newName: "IX_Enquiry_propertyID");

            migrationBuilder.AddForeignKey(
                name: "FK_Enquiry_Property_propertyID",
                table: "Enquiry",
                column: "propertyID",
                principalTable: "Property",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
