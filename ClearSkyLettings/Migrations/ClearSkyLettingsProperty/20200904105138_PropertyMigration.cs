﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace ClearSkyLettings.Migrations.ClearSkyLettingsProperty
{
    public partial class PropertyMigration : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Property",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Title = table.Column<string>(nullable: true),
                    ListingDate = table.Column<DateTime>(nullable: false),
                    AvailableDate = table.Column<DateTime>(nullable: false),
                    Type = table.Column<string>(nullable: true),
                    Beds = table.Column<int>(nullable: false),
                    Bathrooms = table.Column<int>(nullable: false),
                    Rent = table.Column<decimal>(type: "decimal(18,2)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Property", x => x.ID);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Property");
        }
    }
}
