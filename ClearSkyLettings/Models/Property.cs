﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace ClearSkyLettings.Models
{
    public class Property
    {
        public int ID { get; set; }

        [Display(Name ="Address")]
        public string Title { get; set; }

        [Display(Name = "Listing Date")]
        [DataType(DataType.Date)]
        public DateTime ListingDate { get; set; }

        [Display(Name = "Available From")]
        [DataType(DataType.Date)]
        public DateTime AvailableDate { get; set; }

        public string Type { get; set; }

        public int Beds { get; set; }

        public int Bathrooms { get; set; }

        [Column(TypeName = "decimal(18,2)")]
        public decimal Rent { get; set; }
    }
}
